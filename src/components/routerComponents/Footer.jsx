import React, { Component } from 'react'
import { Grid, Row, Col } from "react-bootstrap";

export default class Footer extends Component {
  render() {
    return (
      <footer>
        <Grid fluid>
          <div className="wrapper">
            <Row>
              <Col sm={4}>
                <section className="copyrights">
                    <h3> GymAR </h3>
                    <p>© All Rights Reserved 2018.</p>
                </section>
              </Col>
              <Col sm={4}>
                <section className="adress">
                  <h4>Mobile Development Course</h4>
                  <p className="location">
                    Universidad de Los Andes - Bogotá D.C, Colombia
                  </p>
                </section>
              </Col>
              <Col sm={4}>
                <section className="adress">
                  <h5>Made with ❤️ By <var>MOS</var> </h5>
                  <p className="developers">
                    Nicolás Cabrera Santiago Cortés
                    <br />
                    Juan M. Dominguez Juan D. González
                  </p>
                </section>
              </Col>
            </Row>
          </div>
        </Grid>
    </footer>
    )
  }
}
