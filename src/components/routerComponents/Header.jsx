import React, { Component } from 'react'
import {Navbar, Nav, NavItem, Button} from 'react-bootstrap';
import { LinkContainer } from "react-router-bootstrap";
import { Link } from 'react-router-dom';

export default class Header extends Component {

	constructor(){
    super();
    this.state={
      
    }
	}
	
	handleLogout = () =>{
		this.props.handleLogout( );
	}
	
  render() {
		return (
			<Navbar inverse collapseOnSelect>
			<Navbar.Header>
				<Navbar.Brand>
					<Link to='/'>GymAR Dashboard</Link>
				</Navbar.Brand>
				<Navbar.Toggle />
			</Navbar.Header>
			<Navbar.Collapse>
				{this.props.logged? 
					<Nav>
					<LinkContainer to='/entrenadores'>
						<NavItem>Información Entrenadores</NavItem>
					</LinkContainer>
					<LinkContainer to='/maquinasyejercicios'>
						<NavItem>Máquinas y Ejercicios</NavItem>
					</LinkContainer>
					<LinkContainer to='/programasyrutinas'>
						<NavItem>Planes y Rutinas</NavItem>
					</LinkContainer>
					<LinkContainer to='/zonasGym'>
						<NavItem>Zonas del Gym</NavItem>
					</LinkContainer>
				</Nav>
					: 
					null
				}					
				<Nav pullRight>
					{!this.props.logged ?
						<LinkContainer to='/login'>
							<NavItem>Ingresar</NavItem>
						</LinkContainer>
						:
						<Button bsStyle="primary" bsSize="large" onClick={this.handleLogout}>Salir</Button>
						
					}
				</Nav>
			</Navbar.Collapse>
		</Navbar>
		)
  }
}
