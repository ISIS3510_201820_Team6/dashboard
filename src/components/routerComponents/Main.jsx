import React, { Component } from 'react'
import { Route, Switch } from "react-router-dom";
import { Grid } from "react-bootstrap";

//Componentes Router
import Home from "../homeComponent/Home";
import Trainer from "../trainerComponents/Trainer";
import MachineExercises from "../machinesExercisesComponents/MachineExercises";
import ProgramRoutines from "../programsRoutinesComponents/ProgramRoutines";
import GymZone from "../gymZonesComponents/GymZone";
import Login from "../loginComponents/Login";

export default class Main extends Component {

  render() {
    return (
      <Grid fluid className='mainViewContainer'>

          {this.props.logged ?
            <Switch>
              <Route exact path='/' component={Home}/>
              <Route exact path='/entrenadores' component={Trainer}/>
              <Route exact path='/maquinasyejercicios' component={MachineExercises}/>
              <Route exact path='/programasyrutinas' component={ProgramRoutines}/>
              <Route exact path='/zonasGym' component={GymZone}/>
            </Switch>
            :
            <Switch>
              <Route exact path='/' component={Home}/>
              <Route exact path='/login' component={() => <Login handleLogin = {this.props.handleLogin}/>}/>
            </Switch>
          }

      </Grid>
    )
  }
}
