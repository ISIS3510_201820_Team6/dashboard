import React, { Component } from 'react';
import { Grid, Row, Col, PageHeader, Image, Button } from "react-bootstrap";
import { Link } from 'react-router-dom';
import YouTube from 'react-youtube';

const opts = {
  height: '390',
  width: '640',
  playerVars: { // https://developers.google.com/youtube/player_parameters
    autoplay: 1
  }
};

export default class Home extends Component {

  _onReady(event) {
    // access to player in all event handlers via event.target
    event.target.pauseVideo();
  }

  render() {
    return (
      <Grid fluid>
        <Row className={'homeRow'}>
          <div className={'imageAlpha'}>
            <Col md={6} xs={12}>
              <Row>
                <Col xs={12}>
                  <PageHeader>
                  <small>Bienvenidos a</small> GymAR DashBoard
                  </PageHeader>;
                </Col>
                <Col xs={12}>
                  <YouTube
                    videoId='LvE5ncQ3j84'
                    opts={opts}
                    onReady={this._onReady} />
                </Col>
              </Row>
            </Col>
            <Col md={6} xs={12}>
              <Row>
                <Image
                  className='imageniOS'
                  src='https://firebasestorage.googleapis.com/v0/b/gymar-ac16b.appspot.com/o/img%2FiosPhone.png?alt=media&token=dd7e42c0-5f6d-4da2-b186-43dc2e183bec'
                  responsive />
              </Row>
            </Col>
          </div>
        </Row>
      </Grid>
    )
  }
}
