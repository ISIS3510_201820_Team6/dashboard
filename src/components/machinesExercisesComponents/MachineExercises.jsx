/* eslint-disable no-loop-func */
import React, { Component } from 'react'
import { Grid, Row, Col, Panel, Label } from "react-bootstrap";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import BarChart from '../charts/BarChart';
import firebase from "firebase";

export default class MachineExercises extends Component {
  constructor(){
    super();
    this.state = {
      ejerciciosMasSolicitados:{ },
      zonasMasSolicitadas:{ },
      ejercicios:[],
      categorias: [],
      initialQueryDate: new Date(Date.now() - 86400000),
      finalQueryDate: new Date( Date.now() + 8640000 )
    }
  }

  componentDidMount(){
    this.getEjercicios([
      this.getEjerciciosSolicitados
    ]);
    this.getEjerciciciosPorCategoria();
  }

  handleInitialDateChange = (date) =>{
    this.setState({initialQueryDate: date}, () =>{
      this.getEjerciciosSolicitados( );
    });
  }

  handleFinalDateChange = (date) => {
    this.setState({finalQueryDate: date}, () =>{
      this.getEjerciciosSolicitados( );
    });
  }

  render() {
    return (
      <Grid fluid>
        <Row>
          <Col xs={6}>
            <h1>Información Máquinas y Ejercicios</h1>
          </Col>
          <Col xs={6}>
            <Row>
              <Col md={6} xs={6}>
                <h5>Fecha Inicial</h5>
                <DatePicker
                  selected={this.state.initialQueryDate}
                  onChange={this.handleInitialDateChange}/>
              </Col>
              <Col md={6} xs={6}>
                <h5>Fecha Final</h5>
                <DatePicker
                  selected={this.state.finalQueryDate}
                  onChange={this.handleFinalDateChange}/>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row>
          <Col md={6} xs={12}>
            <Panel>
              <Panel.Body>
                <BarChart chartData={this.state.ejerciciosMasSolicitados} 
                  location="Massachusetts" 
                  legendPosition="bottom"
                  title="Solicitudes por Máquinas"
                  />
              </Panel.Body>
            </Panel>
          </Col>
          <Col md={6} xs={12}>
          <Col md={4} xs={4}>
            <Panel bsStyle={'warning'}>
              <Panel.Body>
                <h3>
                  <Label bsStyle='info'>{this.state.ejercicios.length}</Label> Ejercicios Disponibles
                </h3>
                {
                  this.state.categorias.map(cate => {
                    return(
                    <h5>
                      <Label bsStyle='info'>{cate}</Label>
                      <br />
                    </h5>);
                  })
                }
              </Panel.Body>
            </Panel>
          </Col>
          </Col>
        </Row>
      </Grid>
    )
  }
  
  getEjercicios = ( functions ) => {
    firebase.firestore().collection("ejercicios")
      .onSnapshot((snapshot) =>{
        this.setState({
          ejercicios: snapshot.docs.map((doc) => doc.get('nombre'))
        }, () => functions.forEach((funct) => funct()));
      });
  }

  getEjerciciciosPorCategoria = ( ) => {
    firebase.firestore().collection("categorias")
      .onSnapshot((snapshot) =>{
        this.setState({
          categorias: snapshot.docs.map((doc) => doc.get('nombre'))
        });
      });
  }

  getEjerciciosSolicitados = () => {
    var promises = [];
    this.state.ejercicios.forEach((ejercicio) => {
      promises.push(firebase.firestore().collection("solicitudesEntrenadores")
        .where("ejercicio","==",ejercicio)
        .where('fechaSolicitud', '>=', this.state.initialQueryDate)
        .where('fechaSolicitud', '<=', this.state.finalQueryDate)
        .get());
    });

    Promise.all(promises).then((results) => {
      this.setState({
        ejerciciosMasSolicitados:{
          labels: this.state.ejercicios,
          datasets:[
            {
              label:"Ejercicios que piden más ayuda",
              data: results.map((querySnap) => querySnap.size),
              backgroundColor:[
                'rgba(255, 99, 132, 0.6)',
                'rgba(54, 162, 235, 0.6)',
                'rgba(255, 206, 86, 0.6)',                  
                'rgba(75, 192, 192, 0.6)',
                'rgba(155, 102, 255, 0.6)',
                'rgba(75, 132, 192, 0.6)',
                'rgba(153, 102, 225, 0.6)',
                'rgba(35, 132, 192, 0.6)',
                'rgba(13, 102, 225, 0.6)'
              ]
            }
          ]
        }
      })
    });
  }
}
