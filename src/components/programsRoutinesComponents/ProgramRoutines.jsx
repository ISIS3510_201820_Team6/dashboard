/* eslint-disable no-loop-func */
import React, { Component } from 'react'
import { Grid, Row, Col, Panel } from "react-bootstrap";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import firebase from 'firebase';
import BarChart from "../charts/BarChart"
export default class ProgramRoutines extends Component {
  
  constructor() {
    super();
    this.state = {
      rutinasMasUsadas: {},
      nombre: ["Acondicionamiento", "Fortalecimiento", "Intensivo", "Tonificacion"],
      losQueJoden: {},
      masObjetivos: {},
      ejerCiciosPopulares: {}
    }
  }

  componentDidMount() {
    this.getRutinasMasUsadas()
    this.getClientesMasJodones()
    this.objetivosMasComunes()
  }
  
  getRutinasMasUsadas() {
    const db = firebase.firestore()
    db.collection("Plan")
      .onSnapshot((snapShot) => {
        var arr = [];
        snapShot.forEach((doc) => {
          doc.data().rutinas.forEach(rut => {
            arr.push(rut.get())
          })
        });
        var obj = {}
        Promise.all(arr).then(res => {
          this.ejerciciosMasAsignados(res)
          res.forEach( doc => {
            let data = doc.data()
            if(obj[data.nombre] == null){
              obj[data.nombre] = 1
            } else{
              obj[data.nombre] += 1
            }
            console.log(doc.data());
          })
          var nombres = []
          var cantidad = []
          for(const nombre in obj){
            nombres.push(nombre);
            cantidad.push(obj[nombre]);
          }
          this.setState({
            rutinasMasUsadas: {
              labels: nombres,
              datasets: [
                {
                  label: "Rutinas mas populares",
                  data: cantidad,
                  backgroundColor: [
                    'rgba(255, 99, 132, 0.6)',
                    'rgba(54, 162, 235, 0.6)'
                  ]
                }
              ]
            }
          })
        })
      });
  }

  objetivosMasComunes() {
    const db = firebase.firestore()
    var losObjetivos = []
    var arregloContador = []
    var cuantos = 0;
    var x
    db.collection("Plan")
      .onSnapshot((snapShot) => {
        snapShot.forEach(doc => {
          losObjetivos.push(doc.data().objetivo)
        })
        for (x in losObjetivos) {
          db.collection("Plan").where("objetivo", "==", losObjetivos[x])
            .onSnapshot((snapShot) => {
              snapShot.forEach(doc => {
                cuantos++
              })
              arregloContador.push(cuantos);
              cuantos = 0;
            })
        }
        console.log(arregloContador)
        console.log(losObjetivos)
        this.setState({
          masObjetivos: {
            labels: losObjetivos,
            datasets: [
              {
                label: "Los objetivos",
                data: arregloContador,
                backgroundColor: [
                  'rgba(255, 99, 132, 0.6)',
                  'rgba(54, 162, 235, 0.6)'
                ]
              }
            ]
          }
        })
      })
  }

  getClientesMasJodones() {
    const db = firebase.firestore()
    var losClientes = []
    var arregloContador = []
    var cuantos = 0;
    var x
    db.collection("usuarios").where("rol", "==", "cliente")
      .onSnapshot((snapshot) => {
        snapshot.forEach(doc => {
          losClientes.push(doc.data().nombre)
        })
        for (x in losClientes) {
          db.collection("solicitudesEntrenadores").where("cliente", "==", losClientes[x])
            .onSnapshot((snapshot) => {
              snapshot.forEach(doc => {
                cuantos++;
              });
              arregloContador.push(cuantos);
              cuantos = 0;
            });
        }
        console.log(arregloContador)
        console.log(losClientes)
        this.setState({
          losQueJoden: {
            labels: losClientes,
            datasets: [
              {
                label: "Clientes qué más solicitan ayuda",
                data: arregloContador,
                backgroundColor: [
                  'rgba(255, 99, 132, 0.6)',
                  'rgba(54, 162, 235, 0.6)'
                ]
              }
            ]
          }
        })
      })
  }

  ejerciciosMasAsignados(rutinas){
    var promEjercicios = []
    rutinas.forEach(el =>{
      var data = el.data()
      console.log(data);
      data.ejercicios.forEach( ej => {
        console.log(ej.ejercicio);
        promEjercicios.push(ej.ejercicio.get());
      })
    })
    var obj = {}
    Promise.all(promEjercicios).then(elEjercicio =>{
      elEjercicio.forEach( elEj => {
        var dataEjercicio = elEj.data()
        if(obj[dataEjercicio.nombre] == null){
          obj[dataEjercicio.nombre] = 1
        } else{
          obj[dataEjercicio.nombre] += 1
        }
      })
      var nombres = []
      var cantidad = []
      for(const nombre in obj){
        nombres.push(nombre);
        cantidad.push(obj[nombre]);
      }
      this.setState({
        ejerCiciosPopulares: {
          labels: nombres,
          datasets: [
            {
              label: "Ejercicios",
              data: cantidad,
              backgroundColor: [
                'rgba(255, 99, 132, 0.6)',
                'rgba(54, 162, 235, 0.6)'
              ]
            }
          ]
        }
      })
    });
  }

  render() {
    return (
      <Grid fluid>
        <Row>
          <Col md={12}>
            <h1>Información Planes y Rutinas</h1>
          </Col>
        </Row>
        
        <Row>
          <Col md={6} xs={12}>
            <Panel>
              <Panel.Body>
                <BarChart chartData={this.state.losQueJoden}
                  location="Massachusetts"
                  legendPosition="bottom"
                  title="Solicitudes de ayuda por clientes"
                />
              </Panel.Body>
            </Panel>
          </Col>
          <Col md={6} xs={12}>
            <Panel>
              <Panel.Body>
                <BarChart chartData={this.state.masObjetivos}
                  location="Massachusetts"
                  legendPosition="bottom"
                  title="Objetivos más pedidios por los clientes"
                />
              </Panel.Body>
            </Panel>
          </Col>
          <Col md={6} xs={12}>
            <Panel>
              <Panel.Body>
                <BarChart chartData={this.state.rutinasMasUsadas}
                  location="Massachusetts"
                  legendPosition="bottom"
                  title="Rutinas populares"
                />
              </Panel.Body>
            </Panel>
          </Col>
          <Col md={6} xs={12}>
            <Panel>
              <Panel.Body>
                <BarChart chartData={this.state.ejerCiciosPopulares}
                  location="Massachusetts"
                  legendPosition="bottom"
                  title="Ejercicios Populares"
                />
              </Panel.Body>
            </Panel>
          </Col>
        </Row>
      </Grid>
    )
  }
}
