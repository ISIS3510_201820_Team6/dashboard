/* eslint-disable no-loop-func */
import React, { Component } from 'react';
import { Grid, Row, Col, Panel, Label } from "react-bootstrap";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import firebase from "firebase";
import PieChart from "../charts/PieChart";
import BarChart from '../charts/BarChart';
import { isNullOrUndefined } from 'util';

export default class GymZone extends Component {
  constructor() {
    super();
    this.state = {
      zonasMasSolicitadas: {},
      entrenadoresZona: {},
      tasaEntrenadoresZona: [],
      promedioAtencionPorZona: [],
      zonas: [],
      initialDateSolicitudes: new Date(Date.now() - 86400000),
      finalDateSolicitudes: new Date( ),
      initialDatePromedio: new Date(Date.now() - 86400000),
      finalDatePromedio: new Date( )
    }
  }

  handleInitialDateChangeSolicitudes = (date) => {
    this.setState({initialDateSolicitudes: date}, () =>{
      this.getZonasMasSolicitadas( );
    });
  }

  handleFinalDateChangeSolicitudes = (date) => {
    this.setState({finalDateSolicitudes: date}, () =>{
      this.getZonasMasSolicitadas( );
    });
  }

  handleInitialDateChangePromedios = (date) => {
    this.setState({initialDatePromedio: date}, () =>{
      this.getPromedioAtencionPorZona( );
    });
  }

  handleFinalDateChangePromedioss = (date) => {
    this.setState({finalDatePromedio: date}, () =>{
      this.getPromedioAtencionPorZona( );
    });
  }

  componentDidMount() {
    this.getZonas( );
  }
  
  render() {
    return (
      <Grid fluid>
        <h1>Información de las Zonas del Gimnasio</h1>
        <Row>
          <Col md={6} xs={12}>
            <Panel>
              <Panel.Body>
                <Row>
                  <Col md={6} xs={6}>
                    <h5>Fecha Inicial</h5>
                    <DatePicker
                      selected={this.state.initialDateSolicitudes}
                      onChange={this.handleInitialDateChangeSolicitudes}/>
                  </Col>
                  <Col md={6} xs={6}>
                    <h5>Fecha Final</h5>
                    <DatePicker
                      selected={this.state.finalDateSolicitudes}
                      onChange={this.handleFinalDateChangeSolicitudes}/>
                  </Col>
                </Row>

                <PieChart chartData={this.state.zonasMasSolicitadas}
                location="Massachusetts"
                legendPosition="bottom"
                title="Número de solicitudes por zona" />
              </Panel.Body>
            </Panel>
          </Col>
          <Col md={6} xs={12}>
            <Panel>
              <Panel.Body>
                <BarChart chartData={this.state.entrenadoresZona}
                  location="Massachusetts"
                  legendPosition="bottom"
                  title="Número de Entrenadores por zona"/>
              </Panel.Body>
            </Panel>
          </Col>
        </Row>
        <Row>
          <Col xs={12}>
            <Panel>
              <Panel.Heading>
                <h1>Resumen Zonas</h1>
              </Panel.Heading>
              <Panel.Body>
                <Row>
                  <Col md={2} xs={6}>
                    <h5>Fecha Inicial</h5>
                    <DatePicker
                      selected={this.state.initialDatePromedio}
                      onChange={this.handleInitialDateChangePromedios}/>
                  </Col>
                  <Col md={2} xs={6}>
                    <h5>Fecha Final</h5>
                    <DatePicker
                      selected={this.state.finalDatePromedio}
                      onChange={this.handleFinalDateChangePromedioss}/>
                  </Col>
                </Row>
                <br />
                <Row>
                  { this.state.tasaEntrenadoresZona.length > 0?
                    this.state.tasaEntrenadoresZona.map((tasa, index) => {
                      return(
                        <Col md={3} xs={6} key={this.state.zonas[index]}>
                          <Panel bsStyle="warning">
                            <Panel.Heading><h3>Zona {this.state.zonas[index]}</h3></Panel.Heading>
                            <Panel.Body>
                              <Row>
                                <Col xs={6}>
                                  <h4>Solicitudes por Entrenador</h4>
                                  <h2>
                                    <Label bsStyle={tasa < 5 ? 'success': (tasa < 8 ? 'warning' : 'danger') }>
                                      {tasa}
                                    </Label>
                                  </h2>
                                </Col>
                                <Col xs={6}>
                                <h4>Tempo Promedio Atención</h4>
                                  {!isNaN(this.state.promedioAtencionPorZona[index]) ?
                                    <h2>
                                      <Label bsStyle={this.state.promedioAtencionPorZona[index] < 4 ? 'success': (this.state.promedioAtencionPorZona[index] < 7 ? 'warning' : 'danger') }>
                                        {Math.round(this.state.promedioAtencionPorZona[index])} min.
                                      </Label>
                                    </h2>
                                    :
                                    <h2>
                                      <Label bsStyle={'default'}>
                                        -
                                      </Label>
                                    </h2>
                                  }
                                </Col>
                              </Row>
                            </Panel.Body>
                          </Panel>
                        </Col>)
                    })
                    :
                    null
                  }
                </Row>
              </Panel.Body>
            </Panel>
          </Col>
        </Row>
      </Grid>
    )
  }

  getZonas( ){
    firebase.firestore().collection("usuarios").where('rol', '==', 'entrenador')
      .onSnapshot((snapshot) => {
        
        if(!snapshot.empty){
          this.setState({
            zonas: snapshot.docs.map(doc => doc.get('zona')).filter((doc, index, self) =>  index === self.indexOf(doc))
          }, () => this.getZonasMasSolicitadas( ));
        }
      });
  }

  getZonasMasSolicitadas = ( ) => {
    var arregloContador= new Array(this.state.zonas.length).fill(0);
    
    firebase.firestore( ).collection('solicitudesEntrenadores')
      .where('fechaSolicitud', '>=', this.state.initialDateSolicitudes)
      .where('fechaSolicitud', '<=', this.state.finalDateSolicitudes)
      .onSnapshot((snapshot) => {
        snapshot.forEach(doc => {
          arregloContador[this.state.zonas.indexOf(doc.get('zona'))]++
        });

        this.setState({
          zonasMasSolicitadas: {
            labels: this.state.zonas,
            datasets: [
              {
                label: 'Solicitudes por Zona', 
                data: arregloContador,
                backgroundColor: [
                  'rgba(255, 99, 132, 0.6)',
                  'rgba(54, 162, 235, 0.6)',
                  'rgba(255, 206, 86, 0.6)'
                ]
              }
            ]
          }
        }, () => this.getEntrenadoresPorZona());
      });
  }

  getEntrenadoresPorZona = ( ) => {
    var arregloContador = new Array(this.state.zonas.length).fill(0);
    firebase.firestore().collection("usuarios").where('rol', '==', 'entrenador')
      .onSnapshot((snapshot) => {
        snapshot.forEach(doc =>{
          arregloContador[this.state.zonas.indexOf(doc.get('zona'))]++
        });

        this.setState({
          entrenadoresZona: {
            labels: this.state.zonas,
            datasets: [{
              label: 'Entrenadores por Zona',
              data: arregloContador,
              backgroundColor: [
                'rgba(255, 99, 132, 0.6)',
                'rgba(54, 162, 235, 0.6)',
                'rgba(255, 206, 86, 0.6)'
              ]
            }]
          }        
        }, () => this.getTasaEntrenadoresPorZona());
      });
  }

  getTasaEntrenadoresPorZona = ( ) => {
    var dataSolicitudes = this.state.zonasMasSolicitadas.datasets[0].data;
    var dataEntrenadores = this.state.entrenadoresZona.datasets[0].data;
    
    if(!isNullOrUndefined(dataSolicitudes) && !isNullOrUndefined(dataEntrenadores)){
          var resultado = new Array(this.state.zonas.length).fill(0);
          resultado = resultado.map((valor, index) => dataSolicitudes[index]/dataEntrenadores[index]);
          this.setState({
            tasaEntrenadoresZona: resultado
          }, () => this.getPromedioAtencionPorZona());
    }
  }

  getPromedioAtencionPorZona = ( ) => {
    var arregloSumador = new Array(this.state.zonas.length).fill(0);
    var arregloContador = new Array(this.state.zonas.length).fill(0);
    firebase.firestore().collection("tiempoDeAtencion")
      .where('inicio', '>=', this.state.initialDatePromedio)
      .where('inicio', '<=', this.state.finalDatePromedio)
      .onSnapshot((snapshot) => {
        snapshot.forEach(doc =>{
          arregloSumador[this.state.zonas.indexOf(doc.get('zona'))] += doc.get('tiempoMins');
          arregloContador[this.state.zonas.indexOf(doc.get('zona'))] ++
        });

        this.setState({
          promedioAtencionPorZona: arregloSumador.map((valor, index) => valor/arregloContador[index])
        });

      });
  }
}
