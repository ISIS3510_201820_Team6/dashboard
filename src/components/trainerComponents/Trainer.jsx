/* eslint-disable no-loop-func */
import React, { Component } from 'react';
import { Grid, Row, Col, Panel, Label } from "react-bootstrap";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import BarChart from '../charts/BarChart';
import firebase from "firebase";

export default class Trainer extends Component {
  constructor() {
    super();
    this.state = {
      solicitudesEntrenadores: { },
      clientesAtendidos: { },
      tasaAtencionEntrenadores: { },
      chats: { },
      entrenadores: [],
      initialQueryDate: new Date(Date.now() - 86400000),
      finalQueryDate: new Date( Date.now() + 8640000 ),
    }
  }

  componentDidMount() {
    this.getEntrenadores( [
      this.getSolicitudesEntrenadores, 
      this.getClientesAtendidos, 
      this.getTasaAtencionPorEntrenador,
      this.getChatsEntrenadores]);
  }

  handleInitialDateChangeTasa = (date) =>{
    this.setState({initialQueryDate: date}, () =>{
      this.getTasaAtencionPorEntrenador( );
      this.getClientesAtendidos( );
    });
  }

  handleFinalDateChangeTasa = (date) => {
    this.setState({finalQueryDate: date}, () =>{
      this.getTasaAtencionPorEntrenador( );
      this.getClientesAtendidos( );
    });
  }
  

  render() {
    return (
      <Grid fluid>
        <Row>
          <Col xs={6}>
            <h1>Información de Entrenedores</h1>
          </Col>
          <Col xs={6}>
            <Row>
              <Col md={6} xs={6}>
                <h5>Fecha Inicial</h5>
                <DatePicker
                  selected={this.state.initialQueryDate}
                  onChange={this.handleInitialDateChangeTasa}/>
              </Col>
              <Col md={6} xs={6}>
                <h5>Fecha Final</h5>
                <DatePicker
                  selected={this.state.finalQueryDate}
                  onChange={this.handleFinalDateChangeTasa}/>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row>
          <Col md={6} sx={12}>
            <Panel>
              <Panel.Body>
                <BarChart chartData={this.state.solicitudesEntrenadores}
                  location="Massachusetts"
                  legendPosition="bottom"
                  title="Solicitudes Realizadas a cada Entrenador"
                />
              </Panel.Body>
            </Panel>
          </Col>
          <Col md={6} sx={12}>
            <Panel>
              <Panel.Body>
                <BarChart chartData={this.state.tasaAtencionEntrenadores}
                  location="Massachusetts"
                  legendPosition="bottom"
                  title="Tiempo Promedio de Atención por Entrenador"
                /> 
              </Panel.Body>
            </Panel>
          </Col>
          <Col md={6} sx={12}>
            <Panel>
              <Panel.Body>
                <BarChart chartData={this.state.clientesAtendidos}
                  location="Massachusetts"
                  legendPosition="bottom"
                  title="Solicitudes Actuales por Cada Entrenador"
                />
              </Panel.Body>
            </Panel>
          </Col>
          <Col md={6} sx={12}>
            <Panel>
              <Panel.Body>
                <BarChart chartData={this.state.chats}
                    location="Massachusetts"
                    legendPosition="bottom"
                    title="Chats actualmente activos por entrenador"/>
              </Panel.Body>
            </Panel>
          </Col>
        </Row>
      </Grid>
    )
  }

  getEntrenadores = (functions) =>{
    firebase.firestore().collection("usuarios").where('rol', '==', 'entrenador')
      .onSnapshot((snapshot) => {
        if(!snapshot.empty){
          this.setState({
            entrenadores: snapshot.docs.map(doc => doc.data())
          }, () => functions.forEach(func => func()));
        }
      });
  }

  getClientesAtendidos = () => {
    var promises = [];
    this.state.entrenadores.forEach( entrenador => {
      promises.push(firebase.firestore().collection("trainerUser")
        .where("nombreEntrenador", "==", entrenador.nombre)
        .where('fechaSolicitud', '>=', this.state.initialQueryDate)
        .where('fechaSolicitud', '<=', this.state.finalQueryDate)
        .get());
    });

    Promise.all(promises).then(results =>{
      this.setState({
        solicitudesEntrenadores: {
          labels: this.state.entrenadores.map(entr => entr.nombre),
          datasets: [
            {
              label: "Número de solicitudes atendidas",
              data: results.map(querySnap => querySnap.size),
              backgroundColor: [
                'rgba(255, 99, 132, 0.6)',
                'rgba(54, 162, 235, 0.6)',
                'rgba(255, 206, 86, 0.6)',
                'rgba(235, 206, 86, 0.6)',
                'rgba(225, 216, 36, 0.6)'
              ]
            }
          ]
        }
      });
    });
  }

  getSolicitudesEntrenadores = () => {
    this.setState({
      clientesAtendidos: {
        labels: this.state.entrenadores.map(entr => entr.nombre),
        datasets: [
          {
            label: "Número de Solicitudes Asignadas",
            data: this.state.entrenadores.map(entr => entr.asignados),
            backgroundColor: [
              'rgba(255, 99, 132, 0.6)',
              'rgba(54, 162, 235, 0.6)',
              'rgba(255, 206, 86, 0.6)',
              'rgba(235, 206, 86, 0.6)',
              'rgba(225, 216, 36, 0.6)'
            ]
          }
        ]
      }
    });
  }

  getTasaAtencionPorEntrenador = () => {
    var promises =[];
    this.state.entrenadores.forEach( (entrenador, index) => {
      promises.push(firebase.firestore().collection("tiempoDeAtencion")
        .where("nombreEntrenador", "==", entrenador.nombre)
        .where('inicio', '>=', this.state.initialQueryDate)
        .where('inicio', '<=', this.state.finalQueryDate).get());
    });

    Promise.all(promises).then((results) => {
      var resulatdos = results.map((querySnap) => {
        return querySnap.empty ? 0 : (querySnap.docs.map((doc) => doc.get('tiempoMins')).reduce((total, current) => {
          return total + current
        })) / querySnap.size;
      });
      this.setState({
        tasaAtencionEntrenadores: {
          labels: this.state.entrenadores.map(entr => entr.nombre),
          datasets: [
            {
              label: "Número de solicitudes atendidas",
              data: resulatdos,
              backgroundColor: [
                'rgba(255, 99, 132, 0.6)',
                'rgba(54, 162, 235, 0.6)',
                'rgba(255, 206, 86, 0.6)',
                'rgba(235, 206, 86, 0.6)',
                'rgba(225, 216, 36, 0.6)'
              ]
            }
          ]
        }
      });
    });

    
    
  }

  getChatsEntrenadores = ( ) => {
    var promises =[];
    this.state.entrenadores.forEach( (entrenador, index) => {
      promises.push(firebase.firestore().collection("chats")
        .where("nombreEntrenador", "==", entrenador.nombre).get());
    });

    Promise.all(promises).then((results) => {
      this.setState({
        chats: {
          labels: this.state.entrenadores.map(entr => entr.nombre),
          datasets: [
            {
              label: "Número de Chats Activos por Entrenador",
              data: results.map((querySnap) => querySnap.size),
              backgroundColor: [
                'rgba(255, 99, 132, 0.6)',
                'rgba(54, 162, 235, 0.6)',
                'rgba(255, 206, 86, 0.6)',
                'rgba(235, 206, 86, 0.6)',
                'rgba(225, 216, 36, 0.6)'
              ]
            }
          ]
        }
      });
    });

  }
}
