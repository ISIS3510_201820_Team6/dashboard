import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { format } from 'url';
import firebase from 'firebase';

import Header from "./components/routerComponents/Header";
import Main from "./components/routerComponents/Main";
import './App.css';

import Footer from './components/routerComponents/Footer';
import { isNullOrUndefined } from 'util';

const INITIAL_STATE = {
  logged: false,
  error: null,
};

class App extends Component {
  
  constructor(){
    super();
    this.state={
      name: "Mi campesina santandereana",
      logged: !isNullOrUndefined(firebase.auth( ).currentUser),
      error: null,
      chartData:{ 
      }      
    }
    this.handleLogin = this.handleLogin.bind(this);
  }

  handleLogin = (email, password) =>{
    firebase.auth( ).signInWithEmailAndPassword(email, password)
    .then(() => {
      this.setState({
        error: null,
        logged: true,
      });
      this.props.history.push('entrenadores');
    })
    .catch(error => {
      console.error('Error al Autenticar');
      this.setState({ error });
    });
  };

  handleLogout = () =>{
    if(this.state.logged || !isNullOrUndefined(firebase.auth( ).currentUser)){
      firebase.auth( ).signOut( )
        .then(( ) => {
          this.setState({...INITIAL_STATE});
          this.props.history.push('login');
        })
        .catch((err) => {
          this.setState({err})
        });
    }
  }

  render() {
    return (
      <div className="App">
        <Header logged={this.state.logged} handleLogout ={this.handleLogout}/>
        <Main logged={this.state.logged} handleLogin = {this.handleLogin}/>
        <Footer />
      </div>
    );
  }
}
export default withRouter(App);
